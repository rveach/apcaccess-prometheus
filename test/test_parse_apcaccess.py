
import os
import pprint
import os
import shutil
import sys
import tempfile

import pytest

from prometheus_client.parser import text_string_to_metric_families

# add scripts to the path
sys.path.append(
    os.path.join(
        os.path.split(
            os.path.dirname(
                os.path.abspath(__file__)
            )
        )[0],
        "bin",
    )
)

from collect_apcaccess import parse_apcaccess_strings, extract_apcaccess_metrics
from collect_apcaccess import main as apcaccess_main

@pytest.fixture
def apcaccess_output_1():
    output_1 = """
        APC      : 001,036,0872
        DATE     : 2019-01-28 15:46:35 -0600
        HOSTNAME : myhostname
        VERSION  : 3.14.12 (29 March 2014) debian
        UPSNAME  : myupsname
        CABLE    : USB Cable
        DRIVER   : USB UPS Driver
        UPSMODE  : Stand Alone
        STARTTIME: 2019-01-28 15:39:07 -0600
        MODEL    : Back-UPS RS 1350MS
        STATUS   : ONLINE
        LINEV    : 122.0 Volts
        LOADPCT  : 10.0 Percent
        BCHARGE  : 100.0 Percent
        TIMELEFT : 65.5 Minutes
        MBATTCHG : 10 Percent
        MINTIMEL : 5 Minutes
        MAXTIME  : 0 Seconds
        SENSE    : Medium
        LOTRANS  : 88.0 Volts
        HITRANS  : 147.0 Volts
        ALARMDEL : 30 Seconds
        BATTV    : 27.3 Volts
        LASTXFER : No transfers since turnon
        NUMXFERS : 0
        TONBATT  : 0 Seconds
        CUMONBATT: 0 Seconds
        XOFFBATT : N/A
        SELFTEST : NO
        STATFLAG : 0x05000008
        SERIALNO : 6R3524T28375
        BATTDATE : 2018-06-08
        NOMINV   : 120 Volts
        NOMBATTV : 24.0 Volts
        NOMPOWER : 810 Watts
        FIRMWARE : 951.e3 .D USB FW:e3
        END APC  : 2019-01-28 15:46:37 -0600
    """
    return output_1


def test_usb_output_strings(apcaccess_output_1):

    results = parse_apcaccess_strings(apcaccess_output_1)
    pprint.pprint(results)
    assert results['ALARMDEL'] == '30 Seconds'
    assert results['APC'] == '001,036,0872'
    assert results['BATTDATE'] == '2018-06-08'
    assert results['BATTV'] == '27.3 Volts'
    assert results['BCHARGE'] == '100.0 Percent'
    assert results['CABLE'] == 'USB Cable'
    assert results['DATE'] == '2019-01-28 15:46:35 -0600'
    assert results['DRIVER'] == 'USB UPS Driver'
    assert results['FIRMWARE'] == '951.e3 .D USB FW:e3'
    assert results['HITRANS'] == '147.0 Volts'
    assert results['HOSTNAME'] == 'myhostname'
    assert results['LASTXFER'] == 'No transfers since turnon'
    assert results['LINEV'] == '122.0 Volts'
    assert results['LOADPCT'] == '10.0 Percent'
    assert results['LOTRANS'] == '88.0 Volts'
    assert results['MAXTIME'] == '0 Seconds'
    assert results['MBATTCHG'] == '10 Percent'
    assert results['MINTIMEL'] == '5 Minutes'
    assert results['MODEL'] == 'Back-UPS RS 1350MS'
    assert results['NOMBATTV'] == '24.0 Volts'
    assert results['NOMINV'] == '120 Volts'
    assert results['NOMPOWER'] == '810 Watts'
    assert results['NUMXFERS'] == '0'
    assert results['SELFTEST'] == 'NO'
    assert results['SENSE'] == 'Medium'
    assert results['SERIALNO'] == '6R3524T28375'
    assert results['STATFLAG'] == '0x05000008'
    assert results['STATUS'] == 'ONLINE'
    assert results['TIMELEFT'] == '65.5 Minutes'
    assert results['TONBATT'] == '0 Seconds'
    assert results['UPSMODE'] == 'Stand Alone'
    assert results['UPSNAME'] == 'myupsname'
    assert results['VERSION'] == '3.14.12 (29 March 2014) debian'
    assert results['XOFFBATT'] == 'N/A'

def test_usb_output_metrics(apcaccess_output_1):

    apc_metrics, apc_labels = extract_apcaccess_metrics(apcaccess_output_1)

    # check labels
    assert apc_labels['hostname'] == 'myhostname'
    assert apc_labels['upsname'] == 'myupsname'
    assert apc_labels['cable'] == 'USB Cable'
    assert apc_labels['driver'] == 'USB UPS Driver'
    assert apc_labels['upsmode'] == 'Stand Alone'
    assert apc_labels['model'] == 'Back-UPS RS 1350MS'
    assert apc_labels['serialno'] == '6R3524T28375'
    assert apc_labels['firmware'] == '951.e3 .D USB FW:e3'

    
    # check float value metrics
    assert apc_metrics['apc_line_volt'][0] == 122.0
    assert apc_metrics['apc_battery_volt'][0] == 27.3
    assert apc_metrics['apc_nom_in_volt'][0] == 120.0
    assert apc_metrics['apc_nom_bat_volt'][0] == 24.0
    assert apc_metrics['apc_nom_power_watt'][0] == 810.0
    assert apc_metrics['apc_load_pct'][0] == 10.0
    assert apc_metrics['apc_bat_chg_pct'][0] == 100.0
    assert apc_metrics['apc_min_bat_chg'][0] == 10
    assert apc_metrics['apc_lo_transfer'][0] == 88
    assert apc_metrics['apc_hi_transfer'][0] == 147
    assert apc_metrics['apc_transfer_count'][0] == 0

    # check elapsed time metrics
    assert apc_metrics['apc_time_left_sec'][0] == 3930
    assert apc_metrics['apc_alarm_delay_sec'][0] == 30
    assert apc_metrics['apc_min_time_left_sec'][0] == 300
    assert apc_metrics['apc_max_time_left_sec'][0] == 0
    assert apc_metrics['apc_bat_sec'][0] == 0
    assert apc_metrics['apc_cum_bat_sec'][0] == 0

    # runtime
    assert apc_metrics['apcupsd_runtime_sec'][0] == 448
    # status
    assert apc_metrics['apc_online'][0] == 1
    # selftest
    assert apc_metrics['apc_selftest'][0] == -1

def test_usb_output_collection(apcaccess_output_1):

    # get temporary directory, verify it exists
    collection_dir = tempfile.mkdtemp()
    assert os.path.isdir(collection_dir)

    # write to file, verify it exists
    prom_collection_file = os.path.join(collection_dir, "test.prom")
    apcaccess_main(prom_collection_file, sample_apcaccess_out=apcaccess_output_1)
    assert os.path.exists(prom_collection_file)

    # read the file
    with open(prom_collection_file, "r") as f:
        prom_collection_out = f.read()

    # rm temporary directory
    shutil.rmtree(collection_dir)
    assert not os.path.isdir(collection_dir)

    collections = {}
    for family in text_string_to_metric_families(prom_collection_out):
        for sample in family.samples:
            collections[sample.name] = sample.value
            print sample
            assert sample.labels['hostname'] == 'myhostname'
            assert sample.labels['upsname'] == 'myupsname'
            assert sample.labels['model'] == 'Back-UPS RS 1350MS'

    # make sure all parsed values were found
    assert collections['apc_line_volt'] == 122.0
    assert collections['apc_battery_volt'] == 27.3
    assert collections['apc_nom_in_volt'] == 120.0
    assert collections['apc_nom_bat_volt'] == 24.0
    assert collections['apc_nom_power_watt'] == 810.0
    assert collections['apc_load_pct'] == 10.0
    assert collections['apc_bat_chg_pct'] == 100.0
    assert collections['apc_min_bat_chg'] == 10
    assert collections['apc_lo_transfer'] == 88
    assert collections['apc_hi_transfer'] == 147
    assert collections['apc_transfer_count'] == 0
    assert collections['apc_time_left_sec'] == 3930
    assert collections['apc_alarm_delay_sec'] == 30
    assert collections['apc_min_time_left_sec'] == 300
    assert collections['apc_max_time_left_sec'] == 0
    assert collections['apc_bat_sec'] == 0
    assert collections['apc_cum_bat_sec'] == 0
    assert collections['apcupsd_runtime_sec'] == 448
    assert collections['apc_online'] == 1
    assert collections['apc_selftest'] == -1
