#! /usr/bin/env python

import argparse
import datetime
import os
import re
import subprocess

from prometheus_client import CollectorRegistry, Gauge, write_to_textfile

def decode_apc_date(date_string):
    date_format = "%Y-%m-%d %H:%M:%S"
    return datetime.datetime.strptime(date_string[:19], date_format)


def parse_apcaccess_strings(apc_text):
    apc_dict = {}

    pattern = re.compile(r"^\s*([\w]+)\s*\:\s([\S ]+)\x0a?\x0d?$", flags=re.MULTILINE)
    matches = re.findall(pattern, apc_text)
    for match in matches:
        apc_dict[match[0]] = match[1]

    return apc_dict


def extract_apcaccess_metrics(apc_text):

    apc_strings = parse_apcaccess_strings(apc_text)

    apc_metrics = {}
    apc_labels = {}

    ## Translate labels
    # HOSTNAME : myhostname
    # UPSNAME  : myupsname
    # CABLE    : USB Cable
    # DRIVER   : USB UPS Driver
    # UPSMODE  : Stand Alone
    # MODEL    : Back-UPS RS 1350MS
    # SERIALNO : 6R3524T28375
    # FIRMWARE : 951.e3 .D USB FW:e3
    check_labels = [
        'HOSTNAME', 'UPSNAME', 'CABLE', 'DRIVER',
        'UPSMODE', 'MODEL', 'SERIALNO', 'FIRMWARE',
    ]
    for this_label in check_labels:
        if this_label in apc_strings:
            apc_labels[this_label.lower()] = apc_strings[this_label]


    ## Metrics with Float Values
    # LINEV    : 122.0 Volts
    # BATTV    : 27.3 Volts
    # NOMINV   : 120 Volts
    # NOMBATTV : 24.0 Volts
    # NOMPOWER : 810 Watts
    # LOADPCT  : 10.0 Percent
    # BCHARGE  : 100.0 Percent
    # MBATTCHG : 10 Percent
    # LOTRANS  : 88.0 Volts
    # HITRANS  : 147.0 Volts
    # NUMXFERS : 0
    float_metrics = {
        'LINEV': ('apc_line_volt', 'current input line voltage'),
        'BATTV': ('apc_battery_volt', 'current battery charge voltage'),
        'NOMINV': ('apc_nom_in_volt', ''),
        'NOMBATTV': ('apc_nom_bat_volt', 'nominal battery voltage'),
        'NOMPOWER': ('apc_nom_power_watt', 'Nominal power output in watts'),
        'LOADPCT': ('apc_load_pct', 'percentage of UPS load capacity'),
        'BCHARGE': ('apc_bat_chg_pct', 'battery capacity charge percentage'),
        'MBATTCHG': ('apc_min_bat_chg', 'minimum battery charge for system shutdown'),
        'LOTRANS': ('apc_lo_transfer', 'input line voltage minimum for transfer'),
        'HITRANS': ('apc_hi_transfer', 'input line voltage maximum for transfer'),
        'NUMXFERS': ('apc_transfer_count', 'transfers to battery since apcupsd startup'),
    }
    for string_key, metric_key in float_metrics.items():
        if string_key in apc_strings:
            apc_metrics[metric_key[0]] = (
                float(apc_strings[string_key].split()[0]),
                metric_key[1],
            )

    ## Elapsed Time Metrics
    # TIMELEFT : 65.5 Minutes
    # ALARMDEL : 30 Seconds
    # MINTIMEL : 5 Minutes
    # MAXTIME  : 0 Seconds
    # TONBATT  : 0 Seconds
    # CUMONBATT: 0 Seconds
    et_metrics = {
        'TIMELEFT': ('apc_time_left_sec', 'remaining time estimate'),
        'ALARMDEL': ('apc_alarm_delay_sec', 'delay before alarm is sounded'),
        'MINTIMEL': ('apc_min_time_left_sec', 'minimum runtime before system shutdown'),
        'MAXTIME': ('apc_max_time_left_sec', 'max battery runtime before shutdown'),
        'TONBATT': ('apc_bat_sec', 'time on battery'),
        'CUMONBATT': ('apc_cum_bat_sec', 'cumulative time on battery'),
    }
    for string_key, metric_key in et_metrics.items():
        if string_key in apc_strings:

            if apc_strings[string_key].split()[1].lower() == 'minutes':
                time_multiplier = 60
            else:
                time_multiplier = 1

            apc_metrics[metric_key[0]] = (
                float(apc_strings[string_key].split()[0]) * time_multiplier,
                metric_key[1],
            )

    ## runtime
    # DATE     : 2019-01-28 15:46:35 -0600
    # STARTTIME: 2019-01-28 15:39:07 -0600
    apcaccess_dt = decode_apc_date(apc_strings['DATE'])  
    apcupsd_start_dt = decode_apc_date(apc_strings['STARTTIME'])
    apc_metrics['apcupsd_runtime_sec'] = (
        (apcaccess_dt - apcupsd_start_dt).total_seconds(),
        "total runtime in seconds",
    )

    # STATUS   : ONLINE
    if 'STATUS' in apc_strings:
        apc_metrics['apc_online'] = (
            int(apc_strings['STATUS'].upper() == 'ONLINE'),
            "Is the status ONLINE",
        )

    # SENSE    : Medium
    if 'SENSE' in apc_strings:
        if apc_strings['SENSE'].upper() == 'HIGH':
            apc_metrics['apc_sensitivity'] = (3, "sensitivity to line voltage fluctuations")
        elif apc_strings['SENSE'].upper() == 'MEDIUM':
            apc_metrics['apc_sensitivity'] = (2, "sensitivity to line voltage fluctuations")
        elif apc_strings['SENSE'].upper() == 'LOW':
            apc_metrics['apc_sensitivity'] = (1, "sensitivity to line voltage fluctuations")
        else:        
            apc_metrics['apc_sensitivity'] = (0, "sensitivity to line voltage fluctuations")
            
    # SELFTEST : NO
    if 'SELFTEST' in apc_strings:
        if apc_strings['SELFTEST'].upper() == 'OK':
            apc_metrics['apc_selftest'] = (0, "self test exit code")
        elif apc_strings['SELFTEST'].upper() == 'BT':
            apc_metrics['apc_selftest'] = (1, "self test exit code")
        elif apc_strings['SELFTEST'].upper() == 'NG':
            apc_metrics['apc_selftest'] = (2, "self test exit code")
        elif apc_strings['SELFTEST'].upper() == 'NO':
            apc_metrics['apc_selftest'] = (-1, "self test exit code")
        else:
            apc_metrics['apc_selftest'] = (3, "self test exit code")


    # APC      : 001,036,0872
    # VERSION  : 3.14.12 (29 March 2014) debian
    # LASTXFER : No transfers since turnon
    # XOFFBATT : N/A
    # STATFLAG : 0x05000008
    # BATTDATE : 2018-06-08
    # END APC  : 2019-01-28 15:46:37 -0600

    # http://www.apcupsd.org/manual/manual.html
    # https://linux.die.net/man/8/apcaccess

    return apc_metrics, apc_labels


def main(prom_collection_file, sample_apcaccess_out=None, apcaccess_path='/sbin/apcaccess'):

    if sample_apcaccess_out:
        apcaccess_out = sample_apcaccess_out
    else:
        apcaccess_p = subprocess.Popen(
            [apcaccess_path],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        apcaccess_out, _ = apcaccess_p.communicate()

    apc_metrics, apc_labels = extract_apcaccess_metrics(apcaccess_out)
    
    registry = CollectorRegistry()

    for metric_name, metric_results in apc_metrics.items():
        metric_gauge = Gauge(
            metric_name,
            metric_results[1],
            ['hostname', 'upsname', 'model'],
            registry=registry
        )
        metric_gauge.labels(
            apc_labels['hostname'],
            apc_labels['upsname'],
            apc_labels['model'],
        ).set(metric_results[0])

    write_to_textfile(prom_collection_file, registry)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="outputfile",
        default="/usr/local/node_exporter/collections/apcups.prom",
        help="Output location for the Prometheus Collection file."
    )
    parser.add_argument(
        "-a",
        "--apcaccess",
        type=str,
        dest="apcaccess",
        default="/sbin/apcaccess",
        help="Specify a custom location for apcaccess executable.",
    )
    args = parser.parse_args()
    main(args.outputfile, apcaccess_path=args.apcaccess)
