apcaccess-prometheus

```
FILE FORMAT
       Both "apcupsd.log" and "apcupsd.status" are simple ascii files with several floating point numbers on one line. The format varies based on the type of UPS that you are using.

       /etc/apcupsd.status
       APC      : time and date of last update
       CABLE    : cable type used
       UPSMODEL : ups type or signal method
       UPSMODE  : tells apcupsd what to check
       SHARE    : if ShareUPS is used, this determines what

       SmartUPS and MatrixUPS Smart Signals
       ULINE    : Current (observed) Input Line Voltage
       MLINE    : Max (observed) Input Line Voltage
       NLINE    : Min (observed) Input Line Voltage
       FLINE    : Line Freq (cycles)
       VOUTP    : UPS Output Voltage
       LOUTP    : Percent Load of UPS Capacity
       BOUTP    : Current Charge Voltage of Batteries
       BCHAR    : Batteries Current Charge Percent of Capacity
       BFAIL    : UNSIGNED INT CODE (ups state)
       UTEMP    : Current UPS Temp. in Degrees Cel.
       DIPSW    : Current DIP switch settings for UPS.

       Newer BackUPS Pro Smart Signals
       ULINE    : Current (observed) Input Line Voltage
       MLINE    : Max (observed) Input Line Voltage
       NLINE    : Min (observed) Input Line Voltage
       FLINE    : Line Freq (cycles)
       VOUTP    : UPS Output Voltage
       LOUTP    : Percent Load of UPS Capacity
       BOUTP    : Current Charge Voltage of Batteries
       BCHAR    : Batteries Current Charge Percent of Capacity
       BFAIL    : UNSIGNED INT CODE (ups state)

       BackUPS Pro and SmartUPS v/s Smart Signals
       LINEFAIL : OnlineStatus
       BATTSTAT : BatteryStatus
       MAINS    : LineVoltageState
       LASTEVNT : LastEventObserved

       BackUPS and NetUPS Simple Signals
       LINEFAIL : OnlineStatus
       BATTSTAT : BatteryStatus
```